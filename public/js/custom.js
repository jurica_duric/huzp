$(document).ready(function(){
    er=false;
    
    var validator = $("#form1").validate({
        rules: {
            q1: {
                required: true,
                maxlength: 100,
            },
            q2: {
                required: true,
            },
            q2_2: {
                required: true,
                maxlength: 25,
            },
            q3: {
                required: true,
            },
            q4: {
                required: true,
            },
            q4_2: {
                required: true,
                maxlength: 25,
            },
            q5: {
                required: true,
            },
            q5_2: {
                required: true,
                maxlength: 25,
            },
            q6: {
                required: true,
            },
            q7: {
                required: true,
            },
            q8: {
                required: true,
            },
            q9: {
                required: true,
            },
            q9_1:{
                required: true,
                maxlength: 25,
            },
            q10: {
                required: true,
            },
            q10_1:{
                required: true,
                maxlength: 25,
            },
            q11: {
                required: true,
            },
            q12: {
                required: true,
            },
            q12_1:{
                required: true,
                maxlength: 25,
            },
            q13: {
                required: true,
            },
            q13_1:{
                required: true,
                maxlength: 100,
            },
            q14: {
                required: true,
            },
            q14_1:{
                required: true,
                maxlength: 100,
            },
            q15: {
                required: true,
            },
            q15_1:{
                required: true,
                maxlength: 100,
            },
            q16: {
                required: true,
            },
            q17: {
                required: true,
            },
            q18: {
                required: true,
            },
            q18_1:{
                required: true,
                maxlength: 100,
            },
            q19: {
                required: true,
            },
            q20: {
                required: true,
            },
            q20_1:{
                required: true,
                maxlength: 100,
            },
            q21: {
                required: true,
            },
            q21_1:{
                required: true,
                maxlength: 100,
            },
            q22: {
                required: true,
            },
            q22_1:{
                required: true,
            },
            q22_2:{
                required: true,
            },
            q22_3:{
                required: true,
            },
            q22_4:{
                required: true,
            },
            q22_5:{
                required: true,
            },
            q23: {
                required: true,
            },
            q24: {
                required: true,
            },
            q25: {
                required: true,
            },
            q25_1:{
                required: true,
                maxlength: 25,
            },
            q26: {
                required: true,
            },
            q26_1:{
                required: true,
                maxlength: 100,
            },
            q27: {
                required: true,
            },
            q28: {
                required: true,
            },
            q28_1:{
                required: true,
                maxlength: 100,
            },
            q29: {
                required: true,
            },
            q30: {
                required: true,
            },
            q31: {
                required: true,
                digits: true,
                min: 18,
            },
        },
        messages: {
            q1: {
                required: "Unesite naziv škole ili fakulteta",
                maxlength: "Unesite manje od 100 znakova",
            }, 
            q2: {
                required: "Odaberite barem jednu opciju",
            },
            q2_2: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q3: {
                required: "Odaberite jednu opciju",
            },
            q4: {
                required: "Odaberite jednu opciju",
            },
            q4_2: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q5: {
                required: "Odaberite jednu opciju",
            },
            q5_2: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q6: {
                required: "Odaberite jednu opciju",
            },
            q7: {
                required: "Odaberite jednu opciju",
            },
            q8: {
                required: "Odaberite jednu opciju",
            },
            q9: {
                required: "Odaberite jednu opciju",
            },
            q9_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q10: {
                required: "Odaberite jednu opciju",
            },
            q10_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q11: {
                required: "Odaberite jednu opciju",
            },
            q12: {
                required: "Odaberite jednu opciju",
            },
            q12_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q13: {
                required: "Odaberite jednu opciju",
            },
            q13_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            }, 
            q14: {
                required: "Odaberite jednu opciju",
            },
            q14_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            },
            q15: {
                required: "Odaberite jednu opciju",
            },
            q15_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            },
            q16: {
                required: "Odaberite jednu opciju",
            },
            q17: {
                required: "Odaberite jednu opciju",
            },
            q18: {
                required: "Odaberite jednu opciju",
            },
            q18_1: {
                required: "Ovo polje je obavezno",
            },
            q19: {
                required: "Odaberite jednu opciju",
            },
            q20: {
                required: "Odaberite jednu opciju",
            },
            q20_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            },
            q21: {
                required: "Odaberite jednu opciju",
            },
            q21_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            },
            q22: {
                required: "Odaberite jednu opciju",
            },
            q22_1: {
                required: "Ovo polje je obavezno",
            },
            q22_2: {
                required: "Ovo polje je obavezno",
            },
            q22_3: {
                required: "Ovo polje je obavezno",
            },
            q22_4: {
                required: "Ovo polje je obavezno",
            },
            q22_5: {
                required: "Ovo polje je obavezno",
            },
            q23: {
                required: "Odaberite jednu opciju",
            },
            q24: {
                required: "Odaberite jednu opciju",
            },
            q25: {
                required: "Odaberite jednu opciju",
            },
            q25_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 25 znakova",
            },
            q26: {
                required: "Odaberite jednu opciju",
            },
            q26_1: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            },
            q27: {
                required: "Odaberite jednu opciju",
            },
            q28: {
                required: "Ovo polje je obavezno",
                maxlength: "Unesite manje od 100 znakova",
            },
            q29: {
                required: "Odaberite jednu opciju",
            },
            q30: {
                required: "Odaberite jednu opciju",
            },
            q31: {
                required: "Ovo polje je obavezno",
                min: "Morate imati najmanje 18 godina",
                digits: "Unesite cijeli broj",
            },


                 
        },
        errorPlacement: function(error, element){
            //error.insertAfter(element)
            if ( (element.prop( "type" ) === "checkbox") || (element.prop( "type" ) === "radio") ) {
                    error.insertAfter( element.parent( "fieldset" ) );
                } else {
                    error.insertAfter( element );
                }
            //alert(element);
        }
    });

/*
    $("#form1").submit(function () {
        rules: {
            q2_2: "required"
        }
        messages: {
            q2_2: {
            required: "Unesite ime"
        }}});
*/
/*
        if (!$("input[name='q2']").is(":checked")) {
            //$(".error").text("Odaberite jednu ili više opcija!").show();
            $("#q2 span").addClass("error").text("Odaberite jednu ili više opcija!");
            //er=true;
            $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            else{
                $("#q2 span").removeClass("error");
                //$(".error").text("").show();
            }
            
            if ($("q2_1").is(":checked")) {
                if ($("q2_2").val().length<1){
                    $("#q2_1 span").addClass("error").text("Ako ste odabrali slobodni unos, polje ne smije ostati prazno");
                }
            }



        if (!$("input[name='q3']").is(":checked")) {
            //$(".error").text("Odaberite jednu opciju!").show();
            $("#q3 span").addClass("error").text("Odaberite jednu ili više opcija!");
            //er=true;
            $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            else{
                //$(".error").text("").show();
                $("#q3 span").removeClass("error");
            }
        /*$("input[name='q2']").each(function() {
            alert( this.value + ":" + this.checked );
        });*/
    //});
    /*$("#2").click(function () {
        if ($("#2").is(":checked")) {
            $("#2_1").removeAttr("disabled");
            $("#2_1").attr("required", "true");
            $("#2_1").focus();
        } else {
            $("#2_1").attr("disabled", "disabled");
            $("#2_1").val("");
        }
    });*/
    $("#q2_1").click(function () {
        if ($("#q2_1").is(":checked")) {
            $("#q2_2").removeAttr("disabled");
            $("#q2_2").focus();
        } else {
            $("#q2_2").attr("disabled", "disabled");
            $("#q2_2").val("");
        }
    });
  /* $("#q2_1").click(function () {
        if ($("#q2_1").is(":checked")) {
            $("#q2_2").removeAttr("disabled");
    
            $("#q2_2").focus();
          /*  $("q2_2").rules( "add", {
                required: true,
                minlength: 1,
                messages: {
                    required: "Required input",
                    minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
                }
                });
            if ($("q2_2").length<1){
                    $("#q2_1 span").addClass("error").text("Ako ste odabrali slobodni unos, polje ne smije ostati prazno");
                }
        } else {
            $("#q2_2").attr("disabled", "disabled");
            $("#q2_2").val("");
        }
    });
    */
    
    $("#q4_1").click(function () {
        if ($("#q4_1").is(":checked")) {
            $("#q4_2").removeAttr("disabled");
            $("#q4_2").attr("required", "true");
            $("#q4_2").focus();
        } else {
            $("#q4_2").attr("disabled", "disabled");
            $("#q4_2").val("");
        }
    });
/*      $("#q5_1").click(function () {
        if ($("#q5_1").prop("checked",true)) {
            $("#q5_2").removeAttr("disabled");
            $("#q5_2").attr("required", "true");
            $("#q5_2").focus();
        } else {
            $("#q5_2").add("disabled", "disabled");
            $("#q5_2").val("");
        }
    });
*/
    $("#q5_1").click(function () {
        if ($("#q5_1").prop("checked",true)) {
            $("#q5_2").removeAttr("disabled");
            $("#q5_2").attr("required", "true");
            $("#q5_2").focus();
        }
    });
    $("#52").click(function () {
        if ($("#52").prop("checked",true)) {
            $("#q5_2").attr("disabled", "disabled");
            $("#q5_2").val("");
        }
    });
    $("#91").click(function () {
        if ($("#91").is(":checked")) {
            $("#99").removeAttr("disabled");
            $("#99").attr("required", "true");
            $("#99").focus();
        } else {
            $("#99").attr("disabled", "disabled");
            $("#99").val("");
        }
    });
    $("#10").click(function () {
        if ($("#10").is(":checked")) {
            $("#100").removeAttr("disabled");
            $("#100").attr("required", "true");
            $("#100").focus();
        } else {
            $("#100").attr("disabled", "disabled");
            $("#100").val("");
        }
    });
    $("#12").click(function () {
        if ($("#12").is(":checked")) {
            $("#120").removeAttr("disabled");
            $("#120").attr("required", "true");
            $("#120").focus();
        } else {
            $("#120").attr("disabled", "disabled");
            $("#120").val("");
        }
    });
    $("#13").click(function () {
        if ($("#13").prop("checked",true)) {
            $("#130").removeAttr("disabled");
            $("#130").attr("required", "true");
            $("#130").focus();
        }
    });
    $("#13_2").click(function () {
        if ($("#13_2").prop("checked",true)) {
            $("#130").attr("disabled", "disabled");
            $("#130").val("");
        }
    });
    $("#14").click(function () {
        if ($("#14").prop("checked",true)) {
            $("#140").removeAttr("disabled");
            $("#140").attr("required", "true");
            $("#140").focus();
        }
    });
    $("#14_2").click(function () {
        if ($("#14_2").prop("checked",true)) {
            $("#140").attr("disabled", "disabled");
            $("#140").val("");
        }
    });
    $("#15").click(function () {
        if ($("#15").prop("checked",true)) {
            $("#150").removeAttr("disabled");
            $("#150").attr("required", "true");
            $("#150").focus();
        }
    });
    $("#15_2").click(function () {
        if ($("#15_2").prop("checked",true)) {
            $("#150").attr("disabled", "disabled");
            $("#150").val("");
        }
    });
    $("#18").click(function () {
        if ($("#18").prop("checked",true)) {
            $("#180").removeAttr("disabled");
            $("#180").attr("required", "true");
            $("#180").focus();
        }
    });
    $("#18_2").click(function () {
        if ($("#18_2").prop("checked",true)) {
            $("#180").attr("disabled", "disabled");
            $("#180").val("");
        }
    });
    $("#20").click(function () {
        if ($("#20").prop("checked",true)) {
            $("#200").removeAttr("disabled");
            $("#200").attr("required", "true");
            $("#200").focus();
        }
    });
    $("#20_2").click(function () {
        if ($("#20_2").prop("checked",true)) {
            $("#200").attr("disabled", "disabled");
            $("#200").val("");
        }
    });
    $("#21").click(function () {
        if ($("#21").prop("checked",true)) {
            $("#210").removeAttr("disabled");
            $("#210").attr("required", "true");
            $("#210").focus();
        }
    });
    $("#21_2").click(function () {
        if ($("#21_2").prop("checked",true)) {
            $("#210").attr("disabled", "disabled");
            $("#210").val("");
        }
    });
    $("#25").click(function () {
        if ($("#25").is(":checked")) {
            $("#250").removeAttr("disabled");
            $("#250").attr("required", "true");
            $("#250").focus();
        } else {
            $("#250").attr("disabled", "disabled");
            $("#250").val("");
        }
    });
    $("#26").click(function () {
        if ($("#26").prop("checked",true)) {
            $("#260").removeAttr("disabled");
            $("#260").attr("required", "true");
            $("#260").focus();
        }
    });
    $("#26_2").click(function () {
        if ($("#26_2").prop("checked",true)) {
            $("#260").attr("disabled", "disabled");
            $("#260").val("");
        }
    });

    $("#7").click(function () {
        if ($("#7").prop("checked",true)) {
            //$("#8").attr("disabled", "disabled");
            $("#q8").removeClass("skip1");
            $("#q9").removeClass("skip1");
            $("#q10").removeClass("skip1");
            $("#q11").removeClass("skip1");
            $("#q12").removeClass("skip1");
            $("#q13").removeClass("skip1");
            $("#q14").removeClass("skip1");
            $("#q15").removeClass("skip1");
            $("#q16").removeClass("skip1");
            $("#q17").removeClass("skip1");
            $("#q18").removeClass("skip1");
            $("#q19").removeClass("skip1");
            $("#q20").removeClass("skip1");
            $("#q21").removeClass("skip1");
            $("#q22").removeClass("skip1");
        }
    });
    $("#7_1").click(function () {
        if ($("#7_1").prop("checked",true)) {
            //$("#8").attr("disabled", "disabled");
            $("#q8").addClass("skip1");
            $("#q9").addClass("skip1");
            $("#q10").addClass("skip1");
            $("#q11").addClass("skip1");
            $("#q12").addClass("skip1");
            $("#q13").addClass("skip1");
            $("#q14").addClass("skip1");
            $("#q15").addClass("skip1");
            $("#q16").addClass("skip1");
            $("#q17").addClass("skip1");
            $("#q18").addClass("skip1");
            $("#q19").addClass("skip1");
            $("#q20").addClass("skip1");
            $("#q21").addClass("skip1");
            $("#q22").addClass("skip1");             

        }
    });
    $("#23").click(function () {
        if ($("#23").prop("checked",true)) {
            $("#q24").removeClass("skip1");
            $("#q25").removeClass("skip1");
            $("#q26").removeClass("skip1");
            $("#q27").removeClass("skip1");
        }
    });
    $("#23_1").click(function () {
        if ($("#23_1").prop("checked",true)) {
            $("#q24").addClass("skip1");
            $("#q25").addClass("skip1");
            $("#q26").addClass("skip1");
            $("#q27").addClass("skip1");
        }
    });
});
