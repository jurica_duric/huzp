const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
var assert = require('assert');
var bodyParser = require('body-parser')

var urlencodedParser = bodyParser.urlencoded({ extended: false })

const sqlite3 = require('sqlite3').verbose();
// app.use(express.static('public'));  - radi probleme, @Jurice - ne dirati
app.use(express.static(path.join(__dirname, 'public')));



router.get('/create',function(req,res){
    // open the database connection
    let db = new sqlite3.Database('./poll.db', (err) => {
    if (err) {
      console.error(err.message);
    }
  });
   
  db.serialize(() => {
    // Queries scheduled here will be serialized.
    db.run('CREATE TABLE IF NOT EXISTS poll (	"ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"Timestamp"	TEXT,"Molimo navedite naziv Vaše škole / fakulteta"	TEXT,"Koju / koje od navedenih IT tehnologija posjedujete Vi osobno ili Vaše kućanstvo?"	TEXT,"Koliko često koristite internet?"	TEXT,"U koju svrhu najčešće koristite internet?"	TEXT,"Koristite li YouTube?"	TEXT,"Ukoliko imate profil na društvenim mrežama, koliko imate prijatelja?"	TEXT,"Koristite li internet za e-kupovinu?" TEXT,"Koliko često kupujete putem interneta?" TEXT,"Što sve kupujete putem interneta?" TEXT,"Kako plaćate robu naručenu putem interneta?" TEXT,"Koliko ste e-kupovina (kupovina putem interneta) obavili u proteklih godinu dana?" TEXT,"Kako sve obavljate e-kupovinu?" TEXT,"Jeste li do sada imali problema sa e-kupovinom?" TEXT,"Znate li kako Vam trgovac garantira da je web stranica sigurna za kupovinu?" TEXT,"Znate li koje su pravne posljedice za kupce prilikom kupovine krivotvorene robe?" TEXT,"Koliko često mijenjate lozinke na stranicama koje koristite?" TEXT,"Jeste li čuli za cyber kriminal i moguće posljedice cyber kriminala, kao što su financijske prevare ili krađe osobnih podataka?" TEXT,"Jeste li se do sada susreli sa pokušajem prevare u e-kupovini?" TEXT,"Molimo navedite s kojim ste pravima upoznati kao kupac u e-kupovini?" TEXT,"Jeste li ikada putem e-kupovine dobili neispravnu robu?" TEXT,"Znate li kome se trebate obratiti u slučaju kada smatrate da su povrijeđena Vaša prava prilikom e-kupovine?" TEXT,"Prije e-kupovine, informiram se o zaštiti privatnosti podataka." INTEGER,"Upoznat/-ta sam sa svojim obvezama i pravima prilikom e-kupovine." INTEGER,"Prije e-kupovine, uvijek pročitam opće uvjete prodaje proizvoda.." INTEGER,"Prije narudžbe proizvoda putem Interneta, informiram se o načinu na koji se koriste moji opći podatci." INTEGER,"Smatram da sam vrlo izložen/-na cyber kriminalu i krađi osobnih podataka." INTEGER,"Jeste li ikada koristili usluge Ubera ili Bolta?" TEXT,"Koliko često koristite usluge Ubera ili Bolta?" TEXT,"Zašto koriste usluge Ubera ili Bolta?" TEXT,"Jeste li imali problema sa korištenjem usluga Ubera ili Bolta?" TEXT,"Kome se najprije trebate obratiti u slučaju problema sa korištenjem usluga Ubera ili Bolta?" TEXT,"Što je to Europski potrošački centar?" TEXT,"Gdje se nalazi Europski potrošački centar Hrvatska?" TEXT,"Spol" TEXT,"Molimo navedite koliko imate godina" TEXT);');
  });
   
  // close the database connection
  db.close((err) => {
    if (err) {
      return console.error(err.message);
    }
  });
  res.sendFile(path.join(__dirname+'/index.html'));
  });

router.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/download', (req, res) => {
  res.download('./poll.db');
});

app.post('/poll', urlencodedParser, function (req, res) {
    var q2string="";
    if(req.body.q2!=undefined){
      if(Array.isArray(req.body.q2) && req.body.q2.length>1){      
        var i;
        for (i = 0; i < req.body.q2.length; i++) {
          q2string += req.body.q2[i] + ",";
        }
        q2string=q2string.slice(0,-1);
      }
      else{
        q2string = req.body.q2;
      }
    }
    
    if(req.body.q2_2!=undefined){
      q2string+="," + req.body.q2_2;
    }
    var q4string="";
    if(req.body.q4!=undefined){
      if(Array.isArray(req.body.q4) && req.body.q4.length>1){      
        var i;
        for (i = 0; i < req.body.q4.length; i++) {
          q4string += req.body.q4[i] + ",";
        }
        q4string=q4string.slice(0,-1);
      }
      else{
        q4string = req.body.q4;
      }
    }
    if(req.body.q4_2!=undefined){
      q4string+="," + req.body.q4_2;
    }

    var q5string=req.body.q5;
    if(req.body.q5_2!=undefined){
      q5string+=" " + req.body.q5_2;
    }
    var q9string="";
    if(req.body.q9!=undefined){
      if(Array.isArray(req.body.q9) && req.body.q9.length>1){      
        var i;
        for (i = 0; i < req.body.q9.length; i++) {
          q9string += req.body.q9[i] + ",";
        }
      }
      q9string=q9string.slice(0,-1);
    }
    else{
      q9string = req.body.q9;
    }
    if(req.body.q9_1!=undefined){
      q9string+="," + req.body.q9_1;
    }

    var q10string="";
    if(req.body.q10!=undefined){
      if(Array.isArray(req.body.q10) && req.body.q10.length>1){      
        var i;
        for (i = 0; i < req.body.q10.length; i++) {
          q10string += req.body.q10[i] + ",";
        }
      }
      q10string=q10string.slice(0,-1);
    }
    else{
      q10string = req.body.q10;
    }
    if(req.body.q10_1!=undefined){
      q10string+="," + req.body.q10_1;
    }

    var q12string="";
    if(req.body.q12!=undefined){
      if(Array.isArray(req.body.q12) && req.body.q12.length>1){      
        var i;
        for (i = 0; i < req.body.q12.length; i++) {
          q12string += req.body.q12[i] + ",";
        }
        q12string=q12string.slice(0,-1);
      }
      else{
        q9string = req.body.q12;
      }      
    }
    
    if(req.body.q12_1!=undefined){
      q12string+="," + req.body.q12_1;
    }
    var q13string=req.body.q13;
    if(req.body.q13_1!=undefined){
      q13string+=" " + req.body.q13_1;
    }
    var q14string=req.body.q14;
    if(req.body.q14_1!=undefined){
      q14string+=" " + req.body.q14_1;
    }
    var q15string=req.body.q15;
    if(req.body.q15_1!=undefined){
      q15string+=" " + req.body.q15_1;
    }
    var q18string=req.body.q18;
    if(req.body.q18_1!=undefined){
      q18string+=" " + req.body.q18_1;
    }
    var q20string=req.body.q20;
    if(req.body.q20_1!=undefined){
      q20string+=" " + req.body.q20_1;
    }
    var q21string=req.body.q21;
    if(req.body.q21_1!=undefined){
      q21string+=" " + req.body.q21_1;
    }
    var q25string="";
    if(req.body.q25!=undefined){
      if(Array.isArray(req.body.q25) && req.body.q25.length>1){      
        var i;
        for (i = 0; i < req.body.q25.length; i++) {
          q25string += req.body.q25[i] + ",";
        }
        q25string=q25string.slice(0,-1);
      }
      else{
        q25string = req.body.q25;
      }
    }
    
    if(req.body.q25_1!=undefined){
      q25string+="," + req.body.q25_1;
    }
    var q26string=req.body.q26;
    if(req.body.q26_1!=undefined){
      q26string+=" " + req.body.q26_1;
    }
    let polldate = new Date();
    let date = polldate.getDate().toString().padStart(2, "0");
    let month = (polldate.getMonth() + 1).toString().padStart(2, "0");
    let year = polldate.getFullYear();
    let hours = polldate.getHours().toString().padStart(2, "0");
    let minutes = polldate.getMinutes().toString().padStart(2, "0");
    let seconds = polldate.getSeconds().toString().padStart(2, "0");
    var currentDate = date + ". " + month + ". " + year + ". " + hours + ":" + minutes + ":" + seconds; 

    res.send('<h1>Hvala što ste ispunili anketu!</h1><a href="/">Za povratak kliknite ovdje</a>');
        
    //let db = new sqlite3.Database('./test.db');
    let db = new sqlite3.Database('./poll.db');

      // insert one row into the langs table
      db.run('INSERT INTO poll VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
      [currentDate, req.body.q1,q2string,req.body.q3,q4string,q5string,req.body.q6,req.body.q7,req.body.q8,q9string,q10string,req.body.q11,q12string,q13string,q14string,q15string,req.body.q16,req.body.q17,q18string,req.body.q19,req.body.q20,q21string,req.body.q22_1,req.body.q22_2,req.body.q22_3,req.body.q22_4,req.body.q22_5,req.body.q23,req.body.q24,q25string,q26string,req.body.q27,req.body.q28,req.body.q29,req.body.q30,req.body.q31 ], function(err) {
        if (err) {
          return console.log(err.message);
        }
        // get the last insert id
        console.log(`Podaci spremljeni u bazu.`);
      });
   
    // close the database connection
    db.close();

  })
router.get('/poll',function(req,res){
  res.sendFile(path.join(__dirname+'/poll.html'));
});

router.get('/sitemap',function(req,res){
  res.sendFile(path.join(__dirname+'/sitemap.html'));
});

//add the router
app.use('/', router);
//app.listen(process.env.port || 3000);

module.exports = app;


